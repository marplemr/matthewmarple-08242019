# Matthew - 08242019
## Installation
1. git clone git@gitlab.com:marplemr/matthewmarple-08242019.git
2. docker-compose up
3. access app at localhost:3000

4. instal gitlab runner
  ```sh
  sudo curl --output /usr/local/bin/gitlab-runner https://gitlab-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-runner-darwin-amd64
  sudo chmod +x /usr/local/bin/gitlab-runner

  gitlab-runner register
  enter (https://gitlab.com)
  ```

## Checklist
- [X] Can run app within Docker without installing Node.js on host
- [ ] Review App for app
- [ ] Review App for test code coverage report

## Demo
<!-- Recommend testing these pages with Chrome Incognito to ensure we can view -->
**Merge Request:** CHANGE ME
**App Review App:** CHANGE ME
**Coverage Review App:** CHANGE ME

## Features
<!-- Any special features / interesting details with your implementation you want to explicitly
note -->

## Security
<!--
List security concerns:
- that have been addressed
- that have *not* been addressed
-->

## Improvements
<!-- What could be added? -->
---

## Other notes
<!-- Optional: Anything else you want to mention -->
